$.ajax({
  method: "GET",
  url: "https://jsonplaceholder.typicode.com/posts"
})
.then(function(response) {
  console.log(response);
  var posts = $("#posts");
  response.forEach(function(post) {
    posts.append(
      "<article>" +
        "<h2>" + post.title + "</h2>" +
        "<p>" + post.body + "</p>" +
      "</article>"
    );
  });
});
