var form = $("#arama_form");
var input = $("#input_ara");
var liste = $("#filmler");

form.on("submit", function(event) {
  event.preventDefault();
  $.ajax({
    url: "http://www.omdbapi.com/?s=" + input.val() + "&apikey=ec6483bd",
    method: "GET"
  })
  .then(function(response) {
    liste.html("");
    response.Search.forEach(function(film) {
      liste.append(
        "<li>" +
          "<h2>" + film.Title + "</h2>" +
          "<img src='" + film.Poster + "'>" +
          "<p>" + film.Year + "</p>" +
        "</li>"
      );
    });
  });
});

