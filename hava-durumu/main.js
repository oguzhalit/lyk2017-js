var city_select = $("#city");
var weather_div = $("#weather");
var loader = $("#loading");

if (localStorage.getItem("selected")) {
  get_weather(localStorage.getItem("selected"));
  city_select.val(localStorage.getItem("selected"));
}

city_select.change(function() {
  var selected = $("#city option:selected").val();
  localStorage.setItem("selected", selected);
  get_weather(selected);
});

function get_weather(city) {
  weather_div.html("");
  loader.show();
  $.ajax({
    url: "http://api.openweathermap.org/data/2.5/weather?q=" + city + ",&lang=tr&units=metric&APPID=0617b0b806920586bbd5d1ff4ef84189",
    method: "GET"
  })
  .then(function(response) {
    loader.hide();
    weather_div.html(response.weather[0].description);
  });
}

