// obje
var obj = {
  isim: "onur",
  soyisim: "çelik"
};

// stringe çevir
var string_obj = JSON.stringify(obj);
console.log(string_obj);

// stringi kaydet
localStorage.setItem("kullanici", string_obj);

// localStorage'dan string al
var get = localStorage.getItem("kullanici");

// stringi objeye çevir
var object_str = JSON.parse(get);
console.log(object_str);
